var wms_layers = [];

var format_CommunesEPCIfinanceurscdvd_0 = new ol.format.GeoJSON();
var features_CommunesEPCIfinanceurscdvd_0 = format_CommunesEPCIfinanceurscdvd_0.readFeatures(json_CommunesEPCIfinanceurscdvd_0, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_CommunesEPCIfinanceurscdvd_0 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_CommunesEPCIfinanceurscdvd_0.addFeatures(features_CommunesEPCIfinanceurscdvd_0);
var lyr_CommunesEPCIfinanceurscdvd_0 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_CommunesEPCIfinanceurscdvd_0, 
                style: style_CommunesEPCIfinanceurscdvd_0,
                interactive: true,
    title: 'Communes EPCI financeurs cdvd<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_0.png" /> CA Bergeracoise<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_1.png" /> CA du Bassin de Brive<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_2.png" /> CA Le Grand Périgueux<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_3.png" /> CC Causses et Vallée de la Dordogne<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_4.png" /> CC de la Vallée de l\'Homme<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_5.png" /> CC des Bastides Dordogne-Périgord<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_6.png" /> CC du Pays de Fénelon<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_7.png" /> CC du Terrassonnais en Périgord Noir Thenon Hautefort<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_8.png" /> CC Haute-Corrèze Communauté<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_9.png" /> CC Midi Corrézien<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_10.png" /> CC Sarlat-Périgord Noir<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_11.png" /> CC Xaintrie Val\'Dordogne<br />\
    <img src="styles/legend/CommunesEPCIfinanceurscdvd_0_12.png" /> <br />'
        });
var format_EPCIfinanceurscdvd_1 = new ol.format.GeoJSON();
var features_EPCIfinanceurscdvd_1 = format_EPCIfinanceurscdvd_1.readFeatures(json_EPCIfinanceurscdvd_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_EPCIfinanceurscdvd_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_EPCIfinanceurscdvd_1.addFeatures(features_EPCIfinanceurscdvd_1);
var lyr_EPCIfinanceurscdvd_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_EPCIfinanceurscdvd_1, 
                style: style_EPCIfinanceurscdvd_1,
                interactive: true,
                title: '<img src="styles/legend/EPCIfinanceurscdvd_1.png" /> EPCI financeurs cdvd'
            });
var format_RivireDordogne_2 = new ol.format.GeoJSON();
var features_RivireDordogne_2 = format_RivireDordogne_2.readFeatures(json_RivireDordogne_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_RivireDordogne_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_RivireDordogne_2.addFeatures(features_RivireDordogne_2);
var lyr_RivireDordogne_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_RivireDordogne_2, 
                style: style_RivireDordogne_2,
                interactive: true,
                title: '<img src="styles/legend/RivireDordogne_2.png" /> Rivière Dordogne'
            });
var format_Dpartements_3 = new ol.format.GeoJSON();
var features_Dpartements_3 = format_Dpartements_3.readFeatures(json_Dpartements_3, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Dpartements_3 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Dpartements_3.addFeatures(features_Dpartements_3);
var lyr_Dpartements_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Dpartements_3, 
                style: style_Dpartements_3,
                interactive: true,
                title: '<img src="styles/legend/Dpartements_3.png" /> Départements'
            });

lyr_CommunesEPCIfinanceurscdvd_0.setVisible(true);lyr_EPCIfinanceurscdvd_1.setVisible(true);lyr_RivireDordogne_2.setVisible(true);lyr_Dpartements_3.setVisible(true);
var layersList = [lyr_CommunesEPCIfinanceurscdvd_0,lyr_EPCIfinanceurscdvd_1,lyr_RivireDordogne_2,lyr_Dpartements_3];
lyr_CommunesEPCIfinanceurscdvd_0.set('fieldAliases', {'id': 'id', '_uid_': '_uid_', 'gid': 'gid', 'nom': 'nom', 'nom_m': 'nom_m', 'insee_com': 'insee_com', 'statut': 'statut', 'population': 'population', 'insee_can': 'insee_can', 'insee_arr': 'insee_arr', 'insee_dep': 'insee_dep', 'insee_reg': 'insee_reg', 'siren_epci': 'siren_epci', 'zone_touri': 'zone_touri', 'epci_fin_cdvd': 'epci_fin_cdvd', });
lyr_EPCIfinanceurscdvd_1.set('fieldAliases', {'id': 'id', 'gid': 'gid', 'code_epci': 'code_epci', 'nom_epci': 'nom_epci', 'type_epci': 'type_epci', });
lyr_RivireDordogne_2.set('fieldAliases', {'id': 'id', 'prec_plani': 'prec_plani', 'prec_alti': 'prec_alti', 'artif': 'artif', 'fictif': 'fictif', 'franchisst': 'franchisst', 'nom': 'nom', 'pos_sol': 'pos_sol', 'regime': 'regime', 'z_ini': 'z_ini', 'z_fin': 'z_fin', 'commune': 'commune', 'code_insee': 'code_insee', 'epci_name': 'epci_name', 'dep_name': 'dep_name', 'reg_name': 'reg_name', });
lyr_Dpartements_3.set('fieldAliases', {'gid': 'gid', 'id': 'id', 'nom_dep_m': 'nom_dep_m', 'nom_dep': 'nom_dep', 'insee_dep': 'insee_dep', 'insee_reg': 'insee_reg', });
lyr_CommunesEPCIfinanceurscdvd_0.set('fieldImages', {'id': 'TextEdit', '_uid_': 'TextEdit', 'gid': 'TextEdit', 'nom': 'TextEdit', 'nom_m': 'TextEdit', 'insee_com': 'TextEdit', 'statut': 'TextEdit', 'population': 'TextEdit', 'insee_can': 'TextEdit', 'insee_arr': 'TextEdit', 'insee_dep': 'TextEdit', 'insee_reg': 'TextEdit', 'siren_epci': 'TextEdit', 'zone_touri': 'TextEdit', 'epci_fin_cdvd': 'TextEdit', });
lyr_EPCIfinanceurscdvd_1.set('fieldImages', {'id': 'TextEdit', 'gid': 'TextEdit', 'code_epci': 'TextEdit', 'nom_epci': 'TextEdit', 'type_epci': 'TextEdit', });
lyr_RivireDordogne_2.set('fieldImages', {'id': 'TextEdit', 'prec_plani': 'TextEdit', 'prec_alti': 'TextEdit', 'artif': 'TextEdit', 'fictif': 'TextEdit', 'franchisst': 'TextEdit', 'nom': 'TextEdit', 'pos_sol': 'TextEdit', 'regime': 'TextEdit', 'z_ini': 'TextEdit', 'z_fin': 'TextEdit', 'commune': 'TextEdit', 'code_insee': 'TextEdit', 'epci_name': 'TextEdit', 'dep_name': 'TextEdit', 'reg_name': 'TextEdit', });
lyr_Dpartements_3.set('fieldImages', {'gid': 'TextEdit', 'id': 'TextEdit', 'nom_dep_m': 'TextEdit', 'nom_dep': 'TextEdit', 'insee_dep': 'TextEdit', 'insee_reg': 'TextEdit', });
lyr_CommunesEPCIfinanceurscdvd_0.set('fieldLabels', {'id': 'no label', '_uid_': 'no label', 'gid': 'no label', 'nom': 'no label', 'nom_m': 'no label', 'insee_com': 'no label', 'statut': 'no label', 'population': 'no label', 'insee_can': 'no label', 'insee_arr': 'no label', 'insee_dep': 'no label', 'insee_reg': 'no label', 'siren_epci': 'no label', 'zone_touri': 'no label', 'epci_fin_cdvd': 'no label', });
lyr_EPCIfinanceurscdvd_1.set('fieldLabels', {'id': 'no label', 'gid': 'no label', 'code_epci': 'no label', 'nom_epci': 'no label', 'type_epci': 'no label', });
lyr_RivireDordogne_2.set('fieldLabels', {'id': 'no label', 'prec_plani': 'no label', 'prec_alti': 'no label', 'artif': 'no label', 'fictif': 'no label', 'franchisst': 'no label', 'nom': 'no label', 'pos_sol': 'no label', 'regime': 'no label', 'z_ini': 'no label', 'z_fin': 'no label', 'commune': 'no label', 'code_insee': 'no label', 'epci_name': 'no label', 'dep_name': 'no label', 'reg_name': 'no label', });
lyr_Dpartements_3.set('fieldLabels', {'gid': 'no label', 'id': 'no label', 'nom_dep_m': 'no label', 'nom_dep': 'no label', 'insee_dep': 'no label', 'insee_reg': 'no label', });
lyr_Dpartements_3.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});